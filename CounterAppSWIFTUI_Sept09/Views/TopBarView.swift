import SwiftUI

struct TopBarView: View {
    // MARK: - ©Global-PROPERTIES
    /*..............................*/
    
    /*..............................*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>VSTACK||
        //............................./
        HStack {
            // MARK: -∂ SFHorizontal lines
            Image(systemName: "line.horizontal.3")
            Spacer()// Spaced horizontally
            
            // MARK: -∂ Today
            Text("Today")
                .kerning(8)
                .font(.system(size: 25, weight: Font.Weight.semibold))
            Spacer()// Spaced horizontally
            
            // MARK: -∂ SFBell fill
            Image(systemName: "bell.badge.fill")

        }//||END__PARENT-VSTACK||
        .foregroundColor(.white)
        .font(.title)
        .shadow(color: Color.black, radius: 5, x: 6, y: 10)
        //............................./
        
    }///-|_End Of body_|
    /*©-----------------------------------------©*/
}// END: [STRUCT]

