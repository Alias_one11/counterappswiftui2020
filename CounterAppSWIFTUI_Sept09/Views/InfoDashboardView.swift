import SwiftUI

struct InfoDashboardView: View {
    // MARK: - ©Global-PROPERTIES
    /*..............................*/
    @EnvironmentObject var manager: StepCountManager
    /*..............................*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>VSTACK||
        //............................./
        HStack {
            
            withAnimation(.spring(response: 0.3, dampingFraction: 0.5,
                                  blendDuration: 0.7)) {
                //__________
                ForEach(0..<2) {
                    //__________
                    
                    // MARK: - Mile, Kcal
                    DataItemView(dataItem: manager.statData[$0])
                    
                    // MARK: -∂ Skinny Rectangle line in between
                    Spacer()// Line spaced horizontally
                    Rectangle()
                        .fill(Color.white)
                        .opacity(0.2)
                        .frame(width: 1, height: 200)
                    Spacer()// Line spaced horizontally
                    
                }//:|_ForEach_|
                
            }///:|_withAnimation_|
    
            /*©---------------------------------©*/
            
            // MARK: - Time
            withAnimation(.spring(response: 0.3, dampingFraction: 0.5,
                                  blendDuration: 0.7)) {
                //__________
                DataItemView(dataItem: manager.statData[2])
            }
            
        }//||END__PARENT-VSTACK||
        
        //............................./
        
    }///-|_End Of body_|
    /*©-----------------------------------------©*/
}// END: [STRUCT]

//            DataItemView(dataItem: data[0])
//            // MARK: - // Line spaced horizontally
//            Spacer()
//            Rectangle()
//                .fill(Color.white)
//                .opacity(0.2)
//                .frame(width: 1, height: 200)
//            Spacer()
//            /**.................................*/
//
//            // MARK: - Kcal
//            /**.................................*/
//            DataItemView(dataItem: data[1])
//            // MARK: - // Line spaced horizontally
//            Spacer()
//            Rectangle()
//                .fill(Color.white)
//                .opacity(0.2)
//                .frame(width: 1, height: 200)
//            Spacer()
//            /**.................................*/
//
//            // MARK: - Time
//            DataItemView(dataItem: data[2])
