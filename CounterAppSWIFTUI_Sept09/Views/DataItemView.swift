import SwiftUI

struct DataItemView: View {
    // MARK: - ©Global-PROPERTIES
    /*..............................*/
    let dataItem: DataItem
    @EnvironmentObject var manager: StepCountManager

    /*..............................*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>VSTACK||
        //............................./
        VStack {
            
            Image(systemName: dataItem.imageName)
                .foregroundColor(dataItem.imageColor)
                .font(.system(size: 48))
                .shadow(color: dataItem.imageColor.opacity(0.5),
                        radius: 10, x: 2, y: 24)
            
            Text(dataItem.value)
                .font(.system(size: 36))
                // - ©The fixedSize() modifier can be used to create a view
                // - that maintains the ideal size of its children both dimensions:
                .fixedSize()
                .frame(width: 60)
                .padding(.top, 20)
                .padding(.bottom, 20)

            Text(dataItem.unit)
                .font(.system(size: 22))

            
        }//||END__PARENT-VSTACK||
        .foregroundColor(.white)
        .shadow(color: Color.black, radius: 5, x: 6, y: 10)

        //............................./
        
    }///-|_End Of body_|
    /*©-----------------------------------------©*/
}// END: [STRUCT]

