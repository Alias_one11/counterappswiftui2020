import SwiftUI

struct ProgressGoalView: View {
    // MARK: - ©Global-PROPERTIES
    /*..............................*/
    @EnvironmentObject var manager: StepCountManager
    /*..............................*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>VSTACK||
        //............................./
        ZStack {
            // MARK: -∂ Circle #1
            Circle()
                .stroke(RadialGradient(
                            gradient: Gradient(
                                colors: [
                                    Color.black.opacity(0.2),
                                    Color.white.opacity(0.3)
                                ]),
                            center: .center, startRadius: 80,
                            endRadius: 300), lineWidth: 40
                )
                .frame(width: 260, height: 260)
                .blur(radius: 1.2)
            
            // MARK: -∂ Circle #2
            Circle()
                .trim(from: 0, to: manager.goalPercentage)
                .stroke(LinearGradient(
                    gradient: Gradient(
                        colors: [ColorConstants.bottomColor2, Color.red]),
                    startPoint: .top, endPoint: .trailing
                ),
                style:
                    StrokeStyle(lineWidth: 40, lineCap: .round))
                .frame(width: 260, height: 260)
                .rotationEffect(.degrees(-90))
                .shadow(color: ColorConstants.bottomColor2, radius: 30)
            
            // MARK: - Child-VStack
            /**.................................*/
            VStack {
                // MARK: -∂ Goal count
                //__________
                Text("Goal: \(getFormattedInt(num: manager.stepData.goal))")
                    .font(.system(size: 18, weight: Font.Weight.bold))
                
                // MARK: -∂ Step count
                    //__________
                
                Text("\(getFormattedInt(num: manager.stepData.count))")
                        .font(.system(size: 56, weight: Font.Weight.bold))
            }///:|_VStack_|
            
            
        }//||END__PARENT-VSTACK||
        .shadow(color: Color.black, radius: 5, x: 3, y: 10)
        .foregroundColor(.white)
        /**.................................*/
        
    }///-|_End Of body_|
    //............................./
    
}
/*©-----------------------------------------©*/

// MARK: -∂ Helper METHODS/FUNCTIONS
/*...................................................*/
func getFormattedInt(num: Int) -> String {
    //__________
    let formatter = NumberFormatter()
    formatter.usesGroupingSeparator = false
    
    guard let result = formatter.string(from: NSNumber(value: num))
    else  { return "" }
    
    return result
}

/*...................................................*/
// END: [STRUCT]
