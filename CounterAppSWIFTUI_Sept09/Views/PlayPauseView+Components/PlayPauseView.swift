import SwiftUI

struct PlayPauseView: View {
    // MARK: - ©Global-PROPERTIES
    /*..............................*/
    @EnvironmentObject var manager: StepCountManager
    /*..............................*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>VSTACK||
        //............................./
        ZStack {
            
            // MARK: -∂ Container of the button with content inside
            RoundedRectangle(cornerRadius: 35)
                .fill(Color.white)
                .frame(width: 170, height: 70)
                .shadow(color: Color.black.opacity(0.6), radius: 10, x: 8, y: 15)
            
            // MARK: - Child-HStack
            /**.................................*/
            StartPauseButton()
            /**.................................*/
            
        }//||END__PARENT-VSTACK||
        // MARK: - onTapGesture: Makes this view tappable
        //@`````````````````````````````````````````````
        .onTapGesture(perform: {
            //__________
            manager.updateData()
        })
        //@-`````````````````````````````````````````````
        //............................./
        
    }///-|_End Of body_|
    /*©-----------------------------------------©*/
}// END: [STRUCT]
