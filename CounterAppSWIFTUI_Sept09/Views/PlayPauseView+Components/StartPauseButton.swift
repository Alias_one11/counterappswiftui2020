import SwiftUI

struct StartPauseButton: View {
    // MARK: - ©Global-PROPERTIES
    /*..............................*/
    @EnvironmentObject var manager: StepCountManager
    /*..............................*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>VSTACK||
        //............................./
        HStack {
            //__________
            ZStack {
                //__________
                // MARK: -∂ Play logo
                Circle()
                    .fill(ColorConstants.bottomColor2)
                    .frame(width: 60, height: 60)
                
                // MARK: -∂ Pause/Play text: start = false-->By default
                Image(systemName: manager.start ? "pause.fill" : "play.fill")
                    .font(.title)
                    .foregroundColor(.white)
            }
            
            // MARK: -∂ Start button
            Text(manager.start ? "Pause" : "Start")
                .font(.system(size: 28))
                .bold()
                // - ©The fixedSize() modifier can be used to create a view
                // - that maintains the ideal size of its children both dimensions:
                .fixedSize()
                .frame(width: 60)
                .padding(.leading, 8)
            
        }//||END__PARENT-VSTACK||
        // - Moves the content above, inside the button to the <--left
        .offset(x: -12)
        //............................./
        
    }///-|_End Of body_|
    /*©-----------------------------------------©*/
    
}// END: [STRUCT]

