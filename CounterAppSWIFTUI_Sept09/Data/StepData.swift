import SwiftUI

import SwiftUI

struct StepData: Identifiable {
    // MARK: - ©Global-PROPERTIES
    /*..............................*/
    var id = UUID().uuidString
    // - ©Int by default when var = 0
    var count = 0, calorieCount = 0, minutesCount = 0
    var mileCount: CGFloat = 0
    var goal: Int
    /*..............................*/
}
