import SwiftUI

class StepCountManager: ObservableObject {
    //__________
    // MARK: - ©Global-PROPERTIES
    /*..............................*/
    @Published var stepData: StepData = StepData(goal: 6000)
    @Published var statData: [DataItem] = data
    @Published var goalPercentage: CGFloat = 0.0
    @Published var start: Bool = false
    /*..............................*/
    
    // MARK: -∂ Helper METHODS/FUNCTIONS
    /*...................................................*/
    func updateData() {
        //__________
        // MARK: -∂ Starts & stops the counter
        start.toggle()
        
        for index in 1...45 {
            //__________
            DispatchQueue.main.asyncAfter(deadline: .now() + Double(index)) {
                //__________
                if self.start {
                    
                    let steps: Int = Int.random(in: 0...300)
                    //__________
                    withAnimation(Animation.spring(response: 0.3, blendDuration: 0.9)) {
                        //__________
                        self.updateStepCount(steps: steps)
                    }
                }
                
            }
        }
    }///:|_updateData_|
    
    func updateStepCount(steps: Int) {
        //__________
            
            DispatchQueue.main.async {
                
                if self.stepData.count + steps < self.stepData.goal {
                    
                    self.stepData.count += steps
                    //__________
                } else {
                    self.stepData.count = self.stepData.goal
                    self.start = false
                }
            }
        
        calculateStats()
        
    }///:|_updateStepCount_|
    
    func calculateStats() {
        //__________
        DispatchQueue.main.async {
            //__________
            self.stepData.mileCount = CGFloat(self.stepData.count) * 0.00029
            self.stepData.calorieCount = Int(CGFloat(self.stepData.count) * 0.036)
            self.stepData.minutesCount = Int(CGFloat(self.stepData.count) * 0.0075)
            
            self.goalPercentage = CGFloat(self.stepData.count) / CGFloat(self.stepData.goal)
            
            self.statData[0].value = String.init(format: "%.2f", self.stepData.mileCount)
            self.statData[1].value = "\(self.stepData.calorieCount)"
            self.statData[2].value = "\(self.stepData.minutesCount)\u{1D0D}"
        }
        
    }///:|_updateStepCount_|
    /*...................................................*/
    
}
