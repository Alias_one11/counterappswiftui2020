import SwiftUI

struct DataItem: Identifiable {
    // MARK: - ©Global-PROPERTIES
    /*=============================*/
    var id = UUID().uuidString
    let imageName: String
    let imageColor: Color
    var value, unit: String
    /*=============================*/
}

let data = [
    DataItem(imageName: "drop.fill", imageColor: Color.blue,
             value: "0", unit: "Mile"),
    DataItem(imageName: "flame.fill", imageColor: Color.orange,
             value: "0", unit: "Kcal"),
    DataItem(imageName: "stopwatch.fill", imageColor: Color.green,
             value: "0\u{1D0D}", unit: "Time")
]
