import SwiftUI

// MARK: - Preview
struct ContentView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        ContentView()//.padding(.all, 100)
        //.preferredColorScheme(.dark)
        //.previewLayout(.sizeThatFits)
        //.previewLayout(.fixed(width: 320, height: 640))
        // The preview below is for like a card
        //.previewLayout(.fixed(width: 440, height: 270))
    }
}

struct ContentView: View {
    // MARK: - ©Global-PROPERTIES
    /*..............................*/
    
    /*..............................*/
    
    /*©-----------------------------------------©*/
    var body: some View {
        
        //||_PARENT__=>VSTACK||
        //............................./
        ZStack {/// STACKING VIEWS ON TOP OF EACH OTHER
            // MARK: -∂ Background gradient
            AngularGradient(gradient: ColorConstants.backgroundGradient,
                            center: .bottomTrailing,
                            startAngle: .degrees(170),
                            endAngle: .degrees(270)
            )
            .blur(radius: 70, opaque: true)
            
            // MARK: - Child-TopBarView
            /**.................................*/
            VStack {
                // MARK: -∂ TopBarView
                TopBarView()
                    .padding(.leading, 20)
                    .padding(.trailing, 20)
                    .padding(.top, 44)
                
                // MARK: -∂ ProgressGoalView
                ProgressGoalView()
                    .padding(.top, 44)
                Spacer()// Spaced vertically

                // MARK: -∂ InfoDashboardView
                InfoDashboardView()
                    .padding(.leading, 60)
                    .padding(.trailing, 60)
                Spacer()// Spaced vertically

                // MARK: -∂ PlayPauseView
                PlayPauseView()
                    .padding(.bottom, 40)
                
            }///:|_VStack_|
            /**.................................*/
            
        }//||END__PARENT-VSTACK||
        .edgesIgnoringSafeArea(.all)
        .environmentObject(StepCountManager())
        //............................./
        
    }///:|_End Of body_|
    /*©-----------------------------------------©*/
    
}// END: [STRUCT]


