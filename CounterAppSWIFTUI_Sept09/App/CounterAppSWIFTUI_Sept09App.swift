//
//  CounterAppSWIFTUI_Sept09App.swift
//  CounterAppSWIFTUI_Sept09
//
//  Created by Jose Martinez on 9/9/20.
//

import SwiftUI

@main
struct CounterAppSWIFTUI_Sept09App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
